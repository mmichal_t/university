﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ado
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly SqlConnection connection = new SqlConnection
              (@"Data Source=ASUSK55VD\SQLEXPRESS;Initial Catalog=Fishing;Integrated Security=True");
        string commandString = string.Empty;
        private SqlCommand command;
        private SqlDataAdapter dataAdapter;
        private DataTable dataTable;
        int count;
        public void performselect()
        {
            commandString = "SELECT * FROM Fishers";
            command = new SqlCommand(commandString, connection);
            dataAdapter = new SqlDataAdapter(command);
            dataTable = new DataTable("Fishers");
            dataAdapter.Fill(dataTable);
            dataGrid.ItemsSource = dataTable.DefaultView;
            count = (int)dataTable.Rows[dataTable.Rows.Count - 1].ItemArray[0];
        }
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            performselect();
            connection.Open();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            commandString = "INSERT INTO Fishers " +
                               "VALUES('" + (count+1).ToString() +
                               "', '" + nicktextBox.Text +
                               "', '" + fishtextBox.Text +
                               "', '" + moneytextBox.Text + "')";
            command = new SqlCommand(commandString, connection);
            command.ExecuteNonQuery();
            performselect();
        }

        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {
            commandString = "DELETE FROM Fishers WHERE FisherID=" + deletetextbox.Text;
            command = new SqlCommand(commandString, connection);
            command.ExecuteNonQuery();
            performselect();
        }

        private void updatebtn_Click(object sender, RoutedEventArgs e)
        {
            commandString = "UPDATE Fishers SET Money=" + moneyupdate.Text + " WHERE FisherID=" + whichone.Text;
            command = new SqlCommand(commandString, connection);
            command.ExecuteNonQuery();
            performselect();
        }
    }
}
