﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqLab
{
    public class Fisher : IComparable<Fisher>
    {
        public string Name { get; set; }
        public int LevelNumber { get; set; }
        public Fisher(string name,int level)
        {
            Name = name;
            LevelNumber = level;
        }
        public Fisher()
        {
             
        }
        public int CompareTo(Fisher other)
        {
            return LevelNumber - other.LevelNumber;
        }
    }

    public class Level : IComparable<Level>
    {
        public string LevelCategory { get; set; }
        public int LevelNumber { get; set; }
        public int CompareTo(Level other)
        {
            return LevelNumber - other.LevelNumber;
        }
    }
    public class FisherTest
    {
        public List<Fisher> fishers
        {
            get
            {
                return new List<Fisher>
                {
                    new Fisher {Name="Michal",LevelNumber=2 },
                    new Fisher {Name="Kuzya",LevelNumber=15 },
                    new Fisher {Name="Anderson",LevelNumber=7 },
                    new Fisher {Name="SergeTheGardener",LevelNumber=9 },
                    new Fisher {Name="Sh0werp0wer",LevelNumber=21 },
                    new Fisher {Name="SorryWithoutRespect",LevelNumber=17 }
                };
            }
        }
        public List<Level> levels
        {
            get
            {
                return new List<Level>
                {
                    new Level {LevelCategory="Amateur",LevelNumber=0 },
                    new Level {LevelCategory="Low",LevelNumber=5 },
                    new Level {LevelCategory="Medium",LevelNumber=10 },
                    new Level {LevelCategory="High",LevelNumber=15 },
                    new Level {LevelCategory="Pro",LevelNumber=20 }
                };
            }
        }
    }
    static class Program
    {
        public static int getMax(int[]array)
        {
            return array.Max();
        }
        static void Main(string[] args)
        {
            var community = new FisherTest();
            var fishers = new List<Fisher>(community.fishers);
            var levels = new List<Level>(community.levels);
            #region levelcategory
            var query = levels.Select(e => new { LevelName = e.LevelCategory, LevelNumber = fishers.Where(f => (f.LevelNumber >= e.LevelNumber && f.LevelNumber<e.LevelNumber+4)).Count(s => true) }).OrderByDescending(e => e.LevelNumber);

            Console.WriteLine("Showing the quantity of fishers of each level category:\n\n");

            foreach (var fisher in query)
            {
                Console.WriteLine("Level category: {0}\t Fishers' count: {1}",fisher.LevelName,fisher.LevelNumber);
            }
            #endregion
            #region usingmax
            Console.WriteLine("\n\nUsing getMax to show the max level:\n");
            int[] levelss = fishers.Select(l=>l.LevelNumber).ToArray();
            Console.WriteLine("Max level is {0}", getMax(levelss));
            #endregion
            #region dictionaryforfishers
            var dicforfishers = new Dictionary<int, Fisher>();
            for (int i = 0; i < fishers.Count; i++)
            {
                dicforfishers.Add(i, fishers[i]);
            }
            #endregion
            #region dictionaryforlevels
            var dicforlevels = new Dictionary<int, Level>();
            for (int i = 0; i < levels.Count; i++)
            {
                dicforlevels.Add(i, levels[i]);
            }
            #endregion
            #region usingdictionary
            Console.WriteLine("\n\nUsing dictionary to show each fisher his level");
            var fisherdict = fishers.Select(fisher => fisher.LevelNumber).ToDictionary(level => level, level => new List<Fisher>(fishers.Where(x=>x.LevelNumber==level)));
            foreach (var f in fisherdict)
            {
                foreach (var l in f.Value)
                {
                    Console.WriteLine(String.Format("Fisher {0} has {1} level", l.Name,f.Key));
                }
            }
            
            #endregion
            #region toarray
            Console.WriteLine("\n\nLevel categories (sorted descending, converted to array) and their ranges:\n");
            var catarr = levels.ToArray().OrderByDescending(c => c.LevelNumber);
            foreach(var cat in catarr)
            {
                Console.WriteLine("Level category: {0}\t Level range:{1}-{2}", cat.LevelCategory, cat.LevelNumber, cat.LevelNumber + 4);
            }
            #endregion
            #region compareto
            Console.WriteLine();
            int counter = 0;
            foreach (var f in fishers)
            {
                counter = 0;
                Console.WriteLine("\nSo,{0}:", f.Name);
                for (int i = 0; i < fishers.Count; i++)
                {
                    if(f.CompareTo(fishers[i])>0)
                    {
                        Console.WriteLine("-has higher level than {0}",  fishers[i].Name);
                        counter++;
                    }
                    if (counter == 0)
                    {
                        Console.WriteLine("Try harder, {0}. There is nobody worse than you", f.Name);
                        break;
                    }
                    if(counter==fishers.Count-1)
                    {
                        Console.WriteLine("Congratulations, {0}. You have the highest level of them all - {1}!", f.Name,f.LevelNumber);
                    }
                }
            }
            #endregion
            Console.ReadKey();
        }
    }
}
