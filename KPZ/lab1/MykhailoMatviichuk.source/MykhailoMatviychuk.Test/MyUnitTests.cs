﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Robot.Common;
using MykhailoMatviychuk;
namespace MykhailoMatviychuk.Test
{
    [TestClass]
    public class MyUnitTests
    {
        [TestMethod]
        public void TestMovement()
        {
            var test_alg = new MichalMatviychukAlgorithm();
            var test_map = new Map();
            var test_robo_position = new Position(100, 100);
            var robots = new List<Robot.Common.Robot>()
                        { new Robot.Common.Robot() { Energy=100,
                        Position= test_robo_position} };
            var command = test_alg.DoStep(robots, 0, test_map);
            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, test_map.MaxPozition);
        }
        [TestMethod]
        public void TestCollect()
        {
            var test_alg = new MichalMatviychukAlgorithm();
            var test_map = new Map();
            var test_robo_position = new Position(5, 5);
            var test_station_position = new Position(5, 5);

            test_map.Stations.Add(new EnergyStation() { Energy = 1000, Position = test_station_position, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                        { new Robot.Common.Robot() { Energy=100,
                        Position= test_robo_position} };
            var command = test_alg.DoStep(robots, 0, test_map);
            Assert.IsTrue(command is CollectEnergyCommand);
        }
        [TestMethod]
        public void Pounce()
        {
            var test_alg = new MichalMatviychukAlgorithm();
            var test_map = new Map();
            var robo1_pos = new Position(0, 0);
            var robo2_pos = new Position(1, 1);
            var robots1 = new List<Robot.Common.Robot>(){ new Robot.Common.Robot() { Energy=100,
                        Position= robo1_pos} };
            var robots2 = new List<Robot.Common.Robot>(){ new Robot.Common.Robot() { Energy=100,
                        Position= robo2_pos} };
            var comand = test_alg.DoStep(robots1, 0, test_map);
            Assert.IsTrue(comand is MoveCommand);
            Assert.AreEqual(robo1_pos, robo2_pos);
        }
        [TestMethod]
        public void WhichStation()
        {
            var test_alg = new MichalMatviychukAlgorithm();
            var test_map = new Map();
            var robo_pos1 = new Position(4, 4);
            var robo_pos = new Position(5, 5);
            var empty_stat_pos = new Position(6, 6);
            var full_stat_pos = new Position(5, 5);
            test_map.Stations.Add(new EnergyStation() { Energy = 0, Position = empty_stat_pos });
            test_map.Stations.Add(new EnergyStation() { Energy = 1000, Position = full_stat_pos });
            var robots1 = new List<Robot.Common.Robot>(){ new Robot.Common.Robot() { Energy=20000,
                        Position= robo_pos1} };
            var robots = new List<Robot.Common.Robot>(){ new Robot.Common.Robot() { Energy=20000,
                        Position= robo_pos} };
            var com = test_alg.DoStep(robots1, 0, test_map);
            Assert.IsFalse(com is MoveCommand);
        }
        [TestMethod]
        public void Author()
        {
            var alg = new MichalMatviychukAlgorithm();
            var map = new Map();
            var robotPos = new Position(55, 55);
            var stationPos = new Position(55, 55);
            var emptu_station = new Position(52, 52);
            var my_robot_pos = new Position(54, 54);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos, RecoveryRate = 10 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = emptu_station, RecoveryRate = 10 });
            Owner A = new Owner { Name = "myRob" };
            Owner B = new Owner { Name = "Michal" };
            var robots = new List<Robot.Common.Robot>()
                        { new Robot.Common.Robot() { Energy=100,
                        Position=robotPos,Owner=A},
                        new Robot.Common.Robot() { Energy=100,
                        Position=my_robot_pos,Owner=B}
                          };
            var command = alg.DoStep(robots, 1, map);
            Assert.AreNotEqual(stationPos, robots[1].Position);
        }
        [TestMethod]
        public void Reproduct()
        {
            var test_alg = new MichalMatviychukAlgorithm();
            var test_map = new Map();
            var robo_pos = new Position(5, 5);
            var robots = new List<Robot.Common.Robot>(){ new Robot.Common.Robot() { Energy=20000,
                        Position= robo_pos} };
            var com = test_alg.DoStep(robots, 0, test_map);
            Assert.IsFalse(com is CreateNewRobotCommand);
        }
        [TestMethod]
        public void Wrong_position()
        {
            var alg = new MichalMatviychukAlgorithm();
            var Map = new Map();
            var RobotPosition = new Position(5, 5);
            var Robot = new List<Robot.Common.Robot>()
                        { new Robot.Common.Robot() { Energy=600,
                        Position=RobotPosition} };
            Assert.IsTrue(Map.IsValid(RobotPosition));
        }
        [TestMethod]
        public void MoreEnergy()
        {
            var test_alg = new MichalMatviychukAlgorithm();
            var test_map = new Map();
            var test_robo_position = new Position(4, 4);
            var test_station_position = new Position(5, 5);
            Robot.Common.Robot newRobo = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(5, 5)
            };

            test_map.Stations.Add(new EnergyStation() { Energy = 1000, Position = test_station_position, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                        { new Robot.Common.Robot() { Energy=100,
                        Position= test_robo_position} };
            //var movement = test_alg.DoStep(robots, 0, test_map);
            //Assert.IsTrue(movement is MoveCommand);
            var command = test_alg.IsStationFree(test_map.Stations[0], newRobo, robots);
            //command.Apply(robots, 0, test_map);
            Assert.AreEqual(command, true);
        }
        [TestMethod]
        public void RobotsAndStations()
        {
            var test_alg = new MichalMatviychukAlgorithm();
            var test_map = new Map();
            var robo_pos = new Position(5, 5);
            var robots = new List<Robot.Common.Robot>(){ new Robot.Common.Robot() { Energy=20000,
                        Position= robo_pos} };
            Assert.AreNotEqual(robots.Count, test_map.Stations.Count);
        }
    }
}
