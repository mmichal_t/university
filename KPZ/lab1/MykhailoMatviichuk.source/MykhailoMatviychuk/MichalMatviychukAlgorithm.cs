﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;
namespace MykhailoMatviychuk
{
    public class UsefulMethods
    {
        public static Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);

                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }

        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }


        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }
        public static bool IsThisStation(Position pos, List<Robot.Common.Robot> robots)
        {
            foreach (var location in robots)
            {
                if (location.Position == pos)
                    return true;
            }
            return false;
        }
    }
    public class MichalMatviychukAlgorithm:IRobotAlgorithm
    {
        public int numberOfRound;
        public int LogRound { get; set; }
        public MichalMatviychukAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            LogRound++;
        }
        public string Author
        {
            get { return "Michal Matviychuk"; }
        }

        public string Description
        {
            get { return "My Robo"; }
        }

        
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if ((movingRobot.Energy > 200) && (robots.Count < map.Stations.Count)&&LogRound<40)
            {
                return new CreateNewRobotCommand();
            }
            Position stationPosition = UsefulMethods.FindNearestFreeStation(robots[robotToMoveIndex], map, robots);

            if (stationPosition == null)
                return null;

            if (DistanceHelper.FindDistance(stationPosition,movingRobot.Position)<=2)
                return new CollectEnergyCommand();
            else
            {

                return new MoveCommand() { NewPosition = stationPosition };
            }
        }
        
    }
}
