﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Lab9
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IServiceDB
    {
        [OperationContract]
        List<Fishers> GetFishers();
        [OperationContract]
        void AddFisher(Fishers Fishers);
    }

    [DataContract]
    public class Fishers
    {
        [DataMember]
        public int FisherID { get; set; }
        [DataMember]
        public string FisherName { get; set; }
        [DataMember]
        public double Fish { get; set; }
        [DataMember]
        public double Money { get; set; }

    }

}
