﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Lab9
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ServiceDB : IServiceDB
    {
        readonly SqlConnection connection = new SqlConnection
              (@"Data Source=ASUSK55VD\SQLEXPRESS;Initial Catalog=Fishing;Integrated Security=True");
        private SqlCommand command;

        public void AddFisher(Fishers Fishers)
        {
            connection.Open();
            command = new SqlCommand("insert into Fishers values ('" +
                Fishers.FisherID + "', '" +
                Fishers.FisherName + "', '" +
                Fishers.Fish + "', '" +
                Fishers.Money + "')",connection);
            command.ExecuteNonQuery();
        }
        public List<Fishers> GetFishers()
        {
            connection.Open();
            command = new SqlCommand("select * from Fishers", connection);
            SqlDataReader datareader = command.ExecuteReader();
            List<Fishers> fishers = new List<Fishers>();
            while(datareader.Read())
            {
                fishers.Add(new Fishers()
                {
                    FisherID = Convert.ToInt32(datareader[0].ToString()),
                    FisherName = datareader[1].ToString(),
                    Fish=Convert.ToDouble(datareader[2].ToString()),
                    Money=Convert.ToDouble(datareader[3].ToString())
                });
            }
            connection.Close();
            return fishers;
        }
    }
}
