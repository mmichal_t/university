﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    public class MyEventArgs
    {
        public int durability { get; private set; }
        public MyEventArgs(int d) { durability = d; }
    }
    class FishingRod
    {
        public delegate void MethodContainer(object sender, MyEventArgs e);
     
        public event MethodContainer CrucialDurability;
        //public double weighttocarry { get; set; }
        private int _durability;
        public int durability
        {
            get
            {
                return _durability;
            }
            set
            {
                _durability = value;
                if (value < 60)
                    if (CrucialDurability != null)
                        CrucialDurability(this, new MyEventArgs(value));
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            FishingRod r1 = new FishingRod {  durability = 100 };
            r1.CrucialDurability+=(object o,MyEventArgs arg)=> Console.WriteLine("Oops. Your rod may have too little durability - {0}. Please repair it, or buy a new one",arg.durability);
            for (int i = 100; i >59; --i)
            {
                Console.WriteLine("Durability={0}", --r1.durability);
            }
            Console.ReadKey();
        }
    }
}
