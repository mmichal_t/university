﻿using Lab5.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    class Serialize
    {
        public static void SerializeData(string fileName, ObservableCollection<Inventory> data)
        {
            var formatter = new DataContractSerializer(typeof(ObservableCollection<Inventory>));
            var stream = new FileStream(fileName, FileMode.Create);
            formatter.WriteObject(stream, data);
            stream.Close();
        }

        public static Inventory DeserializeItem(string fileName)
        {
            var formatter = new DataContractSerializer(typeof(ObservableCollection<Inventory>));
            var stream = new FileStream(fileName, FileMode.Open);
            return (Inventory)formatter.ReadObject(stream);
        }
    }
}
