﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Lab5;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Lab5.Model
{
    [DataContract]
    public class Inventory : INotifyPropertyChanged
    {
        private string name;
        private int price;
        private string pricetype;
        public event PropertyChangedEventHandler PropertyChanged;

        [DataMember]
        public string Name { get { return name; } set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        [DataMember]
        public int Price { get
            {
                return price;
            }

            set {
                price = value;
                OnPropertyChanged("Price");
            }
        }
        [DataMember]
        public string PriceType
        {
            get
            {
                return pricetype;
            }

            set
            {
                pricetype = value;
                OnPropertyChanged("PriceType");
            }
        }
        public Inventory(string name, int price)
        {
            Name = name;
            Price = price;
            PriceType = price.ToString();
        }

        public override string ToString()
        {
            return Name + "\t\t" + Price+"\t\t"+PriceType;
        }
        protected void OnPropertyChanged([CallerMemberName]string propertyName=null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
