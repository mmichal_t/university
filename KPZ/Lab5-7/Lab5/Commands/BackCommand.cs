﻿using Lab5.Model;
using Lab5.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Lab5.Commands
{
    class BackCommand:ICommand
    {
        public ObservableCollection<Inventory> inventar { get; set; }
        private ShopWindowViewModel swvm;
        public event EventHandler CanExecuteChanged;
        public BackCommand(ShopWindowViewModel m,ObservableCollection<Inventory> inv)
        {
            this.swvm = m;
            inventar = inv;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            swvm.BackClick();
            Serialize.SerializeData(@"D:\inventar.xml", inventar);
        }
    }
}
