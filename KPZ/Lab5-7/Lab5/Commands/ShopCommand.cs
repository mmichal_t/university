﻿using Lab5.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Lab5.Commands
{
    class ShopCommand : ICommand
    {
        private MainWindowViewModel mwvm;
        public event EventHandler CanExecuteChanged;
        public ShopCommand(MainWindowViewModel m)
        {
            this.mwvm = m;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            mwvm.ShopClick();
        }
    }
}
