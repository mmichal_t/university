﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Lab5.ViewModels;
namespace Lab5.Commands
{
    class ExitCommand : ICommand
    {
        private MainWindowViewModel mwvm;
        public event EventHandler CanExecuteChanged;
        public ExitCommand(MainWindowViewModel m)
        {
            this.mwvm = m;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            mwvm.ExitClick();
        }
    }
}
