﻿using Lab5.Commands;
using Lab5.Model;
using Lab5.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Lab5.ViewModels
{
    class ShopWindowViewModel
    {
        public ObservableCollection<Inventory> inventar { get; set; }
        public BackCommand backcommand { get; set; }
        public ICommand backbutton { get { return backcommand; } }

        public void BackClick()
        {
            MainWindow mw = new MainWindow();
            mw.Show();
            Application.Current.Windows[0].Close();
            
        }
        public ShopWindowViewModel()
        {
            inventar = new ObservableCollection<Inventory>
            {
                new Inventory("Вудка Shimano SS",250),
                new Inventory("Котушка Shimano SuperReel",400),
                new Inventory("Жилка Trilene Hightest 100m",220),
                new Inventory("Гачки Cobra #6 10pcs",50),
                new Inventory("Опариші білі",10)
            };
           // inventar.Add(Serialize.DeserializeItem(@"D:\inventar.xml"));
            
            backcommand = new BackCommand(this,inventar);
        }
    }
}
