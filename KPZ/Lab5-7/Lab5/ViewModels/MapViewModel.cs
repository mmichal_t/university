﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.ViewModels
{
    class MapViewModel:INotifyPropertyChanged
    {
        private string _Position_x;
        private string _Position_y;


        public string Position_X
        {
            get { return _Position_x; }
            set
            {
                _Position_x = value;
                OnPropertyChanged("Position_X");
            }
        }
        
        public string Position_y
        {
            get { return _Position_y; }
            set
            {
                _Position_y = value;
                OnPropertyChanged("Position_y");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
