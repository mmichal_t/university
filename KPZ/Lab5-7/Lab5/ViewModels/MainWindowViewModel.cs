﻿using Lab5.Commands;
using Lab5.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Lab5.ViewModels
{
    class MainWindowViewModel
    {
        private ShopCommand shopcommand;
        private ExitCommand exitcommand;
        public MainWindowViewModel()
        {
            shopcommand = new ShopCommand(this);
            exitcommand = new ExitCommand(this);
        }
        public void ShopClick()
        {
            ShopWindow sw = new ShopWindow();
            sw.Show();
            Application.Current.MainWindow.Close();
        }
        public  void ExitClick()
        {
            Application.Current.Shutdown();
        }
        public ICommand shopbutton { get { return shopcommand; } }
        public ICommand exitbutton { get { return exitcommand; } }
    }
}
