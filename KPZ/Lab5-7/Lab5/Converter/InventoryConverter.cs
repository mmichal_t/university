﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Lab5.Converter
{
    class InventoryConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var PriceType = Int32.Parse(value.ToString());
            if (PriceType < 300)
                return "Cheap tackle";
            else if (PriceType > 300 && PriceType < 800)
                return "Average price of tackle";
            else return "Expensive tackle";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
