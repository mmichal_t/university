﻿using System;
using Ninject;
using Ninject.Modules;
namespace Lab10
{
    class Program
    {
        public static IKernel kernel;

        static void Main(string[] args)
        {
            kernel = new StandardKernel(new Fisher1VSBigFish());
            var hit1 = kernel.Get<IFish>();
            var hit2 = kernel.Get<IFisher>();
            hit2.PullFish();
            hit1.BreakTackles();
            hit1.NoBreakTackles();

            kernel = new StandardKernel(new Fisher2VSVeryBigFish());
            var hit3 = kernel.Get<IFish>();
            var hit4 = kernel.Get<IFisher>();
            hit4.PullFish();
            hit3.BreakTackles();
            hit3.NoBreakTackles();

            Console.ReadKey();
        }
    }

    public interface IFish
    {
        void BreakTackles();
        void NoBreakTackles();
    }
    public interface IFisher
    {
        void PullFish();
    }
    public class BigFish : IFish
    {
        public void BreakTackles()
        {
            Console.WriteLine("What a loser you are! That fish was not so big...");
        }

        public void NoBreakTackles()
        {
            Console.WriteLine("Fisher collected fish");
        }
    }
    public class VeryBigFish : IFish
    {
        public void BreakTackles()
        {
            Console.WriteLine("The very big fish broke tackles. OOPS");
        }

        public void NoBreakTackles()
        {
            Console.WriteLine("And that struggling lasts for 228 hours...");
        }
    }
    public class Fisher1 : IFisher
    {
        public void PullFish()
        {
            Console.WriteLine("Fisher1 threw the bait ");
        }
    }
    public class Fisher2 : IFisher
    {
        public void PullFish()
        {
            Console.WriteLine("\nFisher2 threw the bait");
        }
    }

    public class Fisher1VSBigFish : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IFish>().To<BigFish>();
            this.Bind<IFisher>().To<Fisher1>();
        }
    }
    public class Fisher2VSVeryBigFish : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IFish>().To<VeryBigFish>();
            this.Bind<IFisher>().To<Fisher2>();
        }
    }

}
