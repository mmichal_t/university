﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace entity
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly FishingEntities db = new FishingEntities();
        int count;
        public MainWindow()
        {
            InitializeComponent();
            performselect();
        }
        void performselect()
        {
            dataGrid.ItemsSource = db.Fishers.ToList();
            count = db.Fishers.Max(fisher=>fisher.FisherID)+1;
        }
        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {
            int delid = Int32.Parse(deletetextbox.Text);
            Fishers delfisher = db.Fishers.First(f => f.FisherID == delid);
            db.Fishers.Remove(delfisher);
            db.SaveChanges();
            performselect();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Fishers newone = new Fishers()
            {
                FisherID = count + 1,
                Nickname = nicktextBox.Text,
                Fish = Convert.ToDecimal(fishtextBox.Text),
                Money = Convert.ToDecimal(moneytextBox.Text)
            };
            db.Fishers.Add(newone);
            db.SaveChanges();
            performselect();
        }

        private void updatebtn_Click(object sender, RoutedEventArgs e)
        {
            int upd = Int32.Parse(whichone.Text);
            Fishers update= db.Fishers.First(f => f.FisherID == upd);
            update.Money = Convert.ToDecimal(moneyupdate.Text);
            db.SaveChanges();
            performselect();
        }
    }
}
