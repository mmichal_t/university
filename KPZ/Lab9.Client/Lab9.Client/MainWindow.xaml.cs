﻿using Lab9.Client.ServiceReferenceFishers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab9.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ServiceDBClient fishers;
        public MainWindow()
        {
            InitializeComponent();
            fishers = new ServiceDBClient();
            dataGrid.ItemsSource = fishers.GetFishers();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            fishers.AddFisher(new Fishers()
            {
                FisherID = fishers.GetFishers().Max(f => f.FisherID)+1,
                FisherName = nicktextBox.Text,
                Fish = Convert.ToDouble(fishtextBox.Text),
                Money=Convert.ToDouble(moneytextBox.Text)
            });
            dataGrid.ItemsSource = fishers.GetFishers();
        }
    }
}
