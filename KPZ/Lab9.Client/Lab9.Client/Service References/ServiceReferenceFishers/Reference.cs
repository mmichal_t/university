﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lab9.Client.ServiceReferenceFishers {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Fishers", Namespace="http://schemas.datacontract.org/2004/07/Lab9")]
    [System.SerializableAttribute()]
    public partial class Fishers : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double FishField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int FisherIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FisherNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double MoneyField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Fish {
            get {
                return this.FishField;
            }
            set {
                if ((this.FishField.Equals(value) != true)) {
                    this.FishField = value;
                    this.RaisePropertyChanged("Fish");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int FisherID {
            get {
                return this.FisherIDField;
            }
            set {
                if ((this.FisherIDField.Equals(value) != true)) {
                    this.FisherIDField = value;
                    this.RaisePropertyChanged("FisherID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FisherName {
            get {
                return this.FisherNameField;
            }
            set {
                if ((object.ReferenceEquals(this.FisherNameField, value) != true)) {
                    this.FisherNameField = value;
                    this.RaisePropertyChanged("FisherName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Money {
            get {
                return this.MoneyField;
            }
            set {
                if ((this.MoneyField.Equals(value) != true)) {
                    this.MoneyField = value;
                    this.RaisePropertyChanged("Money");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReferenceFishers.IServiceDB")]
    public interface IServiceDB {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceDB/GetFishers", ReplyAction="http://tempuri.org/IServiceDB/GetFishersResponse")]
        Lab9.Client.ServiceReferenceFishers.Fishers[] GetFishers();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceDB/AddFisher", ReplyAction="http://tempuri.org/IServiceDB/AddFisherResponse")]
        void AddFisher(Lab9.Client.ServiceReferenceFishers.Fishers Fishers);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceDBChannel : Lab9.Client.ServiceReferenceFishers.IServiceDB, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceDBClient : System.ServiceModel.ClientBase<Lab9.Client.ServiceReferenceFishers.IServiceDB>, Lab9.Client.ServiceReferenceFishers.IServiceDB {
        
        public ServiceDBClient() {
        }
        
        public ServiceDBClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceDBClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceDBClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceDBClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Lab9.Client.ServiceReferenceFishers.Fishers[] GetFishers() {
            return base.Channel.GetFishers();
        }
        
        public void AddFisher(Lab9.Client.ServiceReferenceFishers.Fishers Fishers) {
            base.Channel.AddFisher(Fishers);
        }
    }
}
