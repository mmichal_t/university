﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace classes
{
    class Program
    {
        public struct Point
        {
            public int x, y;

            public Point(int v1, int v2) : this()
            {
                this.x = v1;
                this.y = v2;
            }
        }
        #region fish
        public abstract class Fish
        {
            public string Name { get; set; }
            public double Weight { get; set; }
            public double Length { get; set; }
            private Point _Position;
            public Point Position
            {
                get
                {
                    return _Position;
                }
                set
                {
                    _Position = value;
                }
            }

            public List<Bait> baits { get; set; }
            public Fish()
            {
                Console.WriteLine("Fish successfully created");
            }
            public Fish(string n,double w,double l, Point pos,List<Bait> b):this()
            {
                Name = n;
                Weight = w;
                Length = l;
                Position = pos;
                baits = b;
                Console.WriteLine("Fish {0} with length - {1} and weight - {2} was successfully created on position ({3};{4})",
                    Name,Length,Weight,Position.x,Position.y);
            }
            public bool isCaught(Point pos)
            {
                if (Position.Equals(pos))
                {
                    return true;
                }
                else return false;
            }
            public virtual void Bites()
            {

            }
        }
        public class Predator:Fish
        {
            public Predator():base()
            {

            }
            public Predator(string n,double w,double l,Point p,List<Bait> b):base(n,w,l,p,b)
            {

            }
            public override void Bites()
            {
                Console.WriteLine("Caution! This fish bites");
            }
            public void BoxingUnboxing(string n, double w, double l, Point p, List<Bait> b)
            {
                object box = new Predator(n, w, l, p, b);
                Console.WriteLine("Variable box created, type - {0}", box.GetType());
                Predator unbox = (Predator)box;
                Console.WriteLine("Variable unbox created, type - {0}", unbox.GetType());
            }
        }
        public class NotPredator:Fish
        {
            public NotPredator():base()
            {

            }
            public NotPredator(string n, double w, double l, Point p, List<Bait> b):base(n,w,l,p,b)
            {

            }
            public override void Bites()
            {
                Console.WriteLine("Don't worry, this fish will never bite you");
            }
        }
        public enum _Fish
        {
            Predator = 1,
            NotPredator = 0
        }
        #endregion
        public interface IBreak
        {
            void LoseDurability(ref int tackleDurability);
        }
        public interface IRepair
        {
            void RestoreDurability();
        }
        public interface IDelete
        {
            void Delete();
        }
        #region tackle
        public abstract class Tackle
        {
            public string Brand { get; set; }
            public Tackle() { }
            public Tackle(string s)
            {
                Brand = s;
            }
        }
        public class Rod : Tackle,IBreak,IRepair
        {
            public int durability { get; set; }
            private double MaxWeight;
            public double maxweight
            {
                get
                {
                    return MaxWeight;
                }
                set
                {
                    MaxWeight = value;
                }
            }
            public bool isBroken=false;
            public Rod(string b,int d,double mw):base(b)
            {
                durability = d;
                maxweight = mw;
                if (durability < 60)
                    isBroken = true;
                Console.WriteLine("Rod {0} with durability {1} and max weight {2} successfully created",Brand,durability,maxweight);
            }

            public void LoseDurability(ref int tackleDurability)
            {
                durability -= tackleDurability;
                Console.WriteLine("You lost {1} durability.New durability = {0}", durability,tackleDurability);
            }

            public void RestoreDurability()
            {
                durability = 100;
                Console.WriteLine("Durability restored, durability={0}", durability);
            }
            public static explicit operator string (Rod obj)
            {
                Console.WriteLine("Explicit convertion from {0} to string completed", obj.maxweight.GetType());
                return obj.maxweight.ToString();
            }
            public static implicit operator double(Rod obj)
            {
                Console.WriteLine("Implicit convertion from {0} to double completed", obj.maxweight.GetType());
                return obj.maxweight;
            }
        }
        public class Reel : Tackle,IRepair,IBreak
        {
            public int durability { get; set; }
            public bool isBroken = false;
            public Reel(string b, int d) : base(b)
            {
                durability = d;
                if (durability < 60)
                    isBroken = true;
                Console.WriteLine("Reel {0} with durability {1}  successfully created",Brand,durability);
            }
            public void LoseDurability(ref int tackleDurability)
            {
                durability -= tackleDurability;
            }

            public void RestoreDurability()
            {
                durability = 100;
            }
        }
        public class Line : Tackle
        {
            public int Length { get; set; }
            private double MaxWeight;
            public double maxweight
            {
                get
                {
                    return MaxWeight;
                }
                set
                {
                    MaxWeight = value;
                }
            }
            public bool isLittle = false;
            public Line(string b, int l, double mw) : base(b)
            {
                Length = l;
                maxweight = mw;
                if (Length < 25)
                    isLittle = true;
                Console.WriteLine("{0}m of line {1} with maxweight {2}  successfully created",Length, Brand, maxweight);
            }
        }
        public class Hook:Tackle
        {
            public string material { get; set; }
            private int _size;
            public int Size
            {
                get
                {
                    return _size;
                }
                set
                {
                    _size = value;
                }
            }
            public Hook() : base() { }
            public Hook(string b,string m,int s):base(b)
            {
                material = m;
                Size = s;
                Console.WriteLine("Hook {0} of size {2} made from {1} successfully created", Brand, material, Size);
            }
        }
        public class Bait:Tackle
        {
           public string type { get; set; }
            public Bait() : base() { }
            public Bait(string b,string t):base(b)
            {
                type = t;
            }
        }
        #endregion
        #region fisher
        public interface IThrow
        {
            Point Throw();
        }
        public interface IPull
        {
            void Pull(Fish f);
        }
        public class Fisher:IPull,IThrow
        {
            private Point thrown;
            public Point Thrown
            {
                get
                {
                    return thrown;
                }
                set
                {
                    Throw();
                }
            }
            private string _name;
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                }
            }
            private Level _level;
            public Level level
            {
                get
                {
                    return _level;
                }
                set
                {
                    _level = value;
                }
            }
            public List<Tackle> tackles { get; set; }
            public Fisher(string name,Level l,List<Tackle> t)
            {
                Name = name;
                level = l;
                tackles = t;
                Console.WriteLine("Fisher {0} successfully created",Name);
            }
           
            public void Pull(Fish f)
            {
                int tmpx, tmpy;
                if(f.isCaught(Throw()))
                {
                    while(f.Position.y>0)
                    {
                        tmpx = f.Position.x;
                        tmpy = f.Position.y;
                        var koef = tmpy / f.Weight;
                        if (tmpy >= 5)
                            f.Position = new Point(tmpx, Convert.ToInt32(tmpy - koef));
                        else f.Position = new Point(tmpx, 0);
                        Console.WriteLine("Pulling... Position ({0},{1})", f.Position.x, f.Position.y);
                    }
                    Console.WriteLine("Congratulation! You caught a {0} of weight {1}", f.Name, f.Weight);
                }
            }

            public Point Throw()
            {
                return new Point(35,27);
            }
        }
        public class Level
        {
            private string _LevelName;
            public string LevelName
            {
                get
                {
                    return _LevelName;
                }
                set
                {
                    _LevelName = value;
                }
            }
            private Dictionary<int, int> Points = new Dictionary<int, int>
            {
                {0,0 },
                {1,100 },
                {2,300 },
                {3,600 },
                {4,1000 },
                {5,1500 },
                {6,2100 },
                {7,2800 },
                {8,3600 },
                {9,4500 },
                {10,5500 },
                {11,6600 },
                {12,7800 },
                {13,9100 },
                {14,10500 },
                {15,12000 }
            };
            public int TakeValue()
            {
                LevelValue = -1;
                for (int i = 0; i < Points.Count; i++)
                {
                    if (LevelPoints > Points.Values.ToArray()[i])
                        LevelValue++;
                }
                return LevelValue;
            }
            public string TakeName()
            {
                if (LevelValue >= 0 && LevelValue < 5)
                    return "Amateur";
                else if (LevelValue >= 5 && LevelValue < 10)
                    return  "Normal";
                else if (LevelValue >= 10 && LevelValue < 15)
                    return  "High";
                else return "Pro";
            }
            private int _LevelValue=0;
            public int LevelValue
            {
                get
                {
                    return _LevelValue;
                }
                set
                {
                    _LevelValue = value;
                }
            }
            
            private int _LevelPoints=0;
            public int LevelPoints
            {
                get
                {
                    return _LevelPoints;
                }
                set
                {
                    _LevelPoints = value;
                }
            }
            public Level(int levelpoints)
            {
                LevelPoints = levelpoints;
                Console.WriteLine("Level - {1} (Level - {0}, Points - {2})", TakeValue(),TakeName(),LevelPoints);
            }
        }
        #endregion
        

        static void Main(string[] args)
        {
            NotPredator carp = new NotPredator("Carp", 8.5, 0.8, new Point(35, 27), new List<Bait>() { new Bait("A", "Worm"), new Bait("B", "Corn") });
            Rod shimano3 = new Rod("Shimano", 100, 15);
            Reel shimano12 = new Reel("Shimano", 100);
            Line super = new Line("SuperLine", 250, 20);
            Hook feeder = new Hook("Feederee", "copper", 5);
            Fisher fisher = new Fisher("Michal", new Level(6800), new List<Tackle>() { shimano3, shimano12, super, feeder });
            fisher.Pull(carp);
            Predator bass = new Predator();
            bass.BoxingUnboxing("Bass", 2.5, 0.5, new Point(35, 27), new List<Bait>() { new Bait("A", "Worm"), new Bait("B", "Fish") });
            var lost = 20;
            shimano3.LoseDurability(ref lost);
            shimano3.RestoreDurability();
            string maxweight = (string)shimano3;
            double maxweigt = shimano3;
            Console.WriteLine("Bass is {0}", _Fish.Predator | _Fish.NotPredator);
            Console.WriteLine("Bass is not {0}", _Fish.Predator & _Fish.NotPredator);
            Console.ReadKey();
        }
    }
}
